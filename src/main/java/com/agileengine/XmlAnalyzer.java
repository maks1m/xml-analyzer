package com.agileengine;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class XmlAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlAnalyzer.class);

    private final XmlParser xmlParser = new XmlParser();

    public Optional<ElementMatch> findMatchElement(String originalFilePath, String sampleFilePath, String elementId) {

        Optional<Element> origElementOpt = xmlParser.findElementById(new File(originalFilePath), elementId);
        if (!origElementOpt.isPresent()) {
            throw new NoSuchElementException(String.format("Element with id %s not found", elementId));
        }

        Element origElement = origElementOpt.get();
        Elements sampleElements = getElementsFromSampleFile(origElement, sampleFilePath);

        return matchElements(origElement, sampleElements);
    }

    private Elements getElementsFromSampleFile(Element origElement, String filePath) {

        String cssQuery = buildCssQuery(origElement);

        LOGGER.info("Looking for sample elements with css query: {}", cssQuery);
        Optional<Elements> sampleElementsOpt = xmlParser.findElementsByQuery(new File(filePath), cssQuery);

        if (!sampleElementsOpt.isPresent() || sampleElementsOpt.get().isEmpty()) {
            throw new NoSuchElementException(
                    String.format("No sample elements were found with css query %s", cssQuery));
        }

        return sampleElementsOpt.get();
    }

    private String buildCssQuery(Element element) {

        String parentQuery = "";

        Optional<Element> elParentOpt = findOrigElementParent(element);

        LOGGER.debug("Parent for original element is found: {}", elParentOpt.isPresent());

        if (elParentOpt.isPresent()) {
            Element parent = elParentOpt.get();
            Attributes attrs = parent.attributes();
            String idValue = attrs.get("id");
            String classValue = attrs.get("class");

            if (!idValue.isEmpty()) {
                parentQuery = String.format("%s[id=\"%s\"]",
                        parent.tag().getName(),
                        idValue);

            } else if (!classValue.isEmpty()) {
                parentQuery = String.format("%s[class=\"%s\"]",
                        parent.tag().getName(),
                        classValue);
            }
        }

        String elementQuery = String.format("%s[class*=\"%s\"]",
                element.tag().getName(),
                element.attributes().get("class").split(" ")[0]);

        return parentQuery + " " + elementQuery;
    }

    private Optional<Element> findOrigElementParent(Element origElement) {

        // this one is the main step to reduce possible sample elements
        Optional<Element> parent = origElement.parents().stream()
                .skip(1)
                .filter(el -> el.children().size() == 1)
                .findFirst();

        parent.ifPresent(element ->
                LOGGER.debug("Parent element is: {}", element.cssSelector()));

        return parent;
    }

    private Optional<ElementMatch> matchElements(Element origElement, List<Element> sampleElements) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("sample elements:");
            sampleElements.forEach(el -> LOGGER.debug(el.cssSelector()));
        }

        ElementHolder origElHolder = buildElementHolder(origElement);

        return sampleElements.stream()
                .map(se -> getMatchScore(origElHolder, buildElementHolder(se)))
                .max(Comparator.comparing(ElementMatch::getMatchScore));
    }

    private ElementMatch getMatchScore(ElementHolder original, ElementHolder sample) {

        List<String> matchAnalysis = new LinkedList<>();
        int matchScore = 0;
        int matchCount = 0;

        if (original.getId().equals(sample.getId())) {
            matchScore = 100;
            matchAnalysis.add("Id is equal");
        } else {
            // compare classes
            for (String cssClass : original.getCssClasses()) {
                if (sample.getCssClasses().contains(cssClass)) {
                    matchCount++;
                    matchAnalysis.add("Identical css class" + cssClass);
                }
            }

            // compare attributes
            for (Map.Entry<String, String> entry : original.getAttributesMap().entrySet()) {
                String origValue = entry.getValue();
                String sampleValue = sample.getAttributesMap().get(entry.getKey());

                if (origValue.equals(sampleValue)) {
                    matchCount++;
                    matchAnalysis.add("Identical " + entry.getKey() + " : " + entry.getValue());
                }
            }
            matchScore = (int) (matchCount * 100.00 / original.getAttributesCount());
        }


        return ElementMatch.builder()
                .elementHolder(sample)
                .matchAnalysis(matchAnalysis)
                .matchScore(matchScore)
                .build();
    }

    private ElementHolder buildElementHolder(Element el) {
        Map<String, String> attrMap = el.attributes().asList().stream()
                .collect(Collectors.toMap(Attribute::getKey, Attribute::getValue));

        attrMap.put("tagValue", el.text());

        Optional<String> strClassOpt = Optional.ofNullable(attrMap.remove("class"));
        HashSet<String> cssClasses = strClassOpt
                .map(c -> new HashSet<>(Arrays.asList(c.split(" "))))
                .orElse(new HashSet<>());

        return ElementHolder.builder()
                .element(el)
                .id(attrMap.get("id"))
                .attributesMap(attrMap)
                .cssClasses(cssClasses)
                .build();
    }

}
