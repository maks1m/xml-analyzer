# AgileEngine backend-XML analyzer

All main logic is located in java class XmlAnalyzer.
To make the code more readable and avoid messy code blocks I've added two additional classes:
ElementHolder - this one helps to separate main fields I need for comparison
ElementMatch - this one helps for result comparing and keeps result analysis.

The basic logic behind the algorithm is quite simple:
In the original file, I look for our element using his ID.
If an element is found then I use some general info to build CSS query which will be used
to find elements in the sample file.

For CSS query I look for element's parent (I skip direct parent and look for the first one which
has only one child, so it will be like wrapper section)
Then I use parent Id or CSS class depending on what is present.
Also, I take CSS class for our element.

In general css query will look like: div[class="col-lg-8"] a[class*="btn"]

With this query, I get a few elements from the sample file.
Then I build attributes map for original elements and sample elements.
Compare them and calculate the match score.
The element which has a higher score is the winner.

I didn't write java docs to save time but I tried to made code more readable with the help of
method segregation.
