package com.agileengine;

import lombok.Builder;
import lombok.Getter;
import org.jsoup.nodes.Element;

import java.util.HashSet;
import java.util.Map;

@Builder
@Getter
public class ElementHolder {

    private Element element;

    private String id;

    private Map<String, String> attributesMap;

    private HashSet<String> cssClasses;

    public int getAttributesCount() {
        return cssClasses.size() + attributesMap.size();
    }

}
