package com.agileengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class Main {

    private static final String DEFAULT_ELEMENT_ID = "make-everything-ok-button";

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void testMe(String[] args) {

        String elementId = "make-everything-ok-button";
        String originalFile = "./samples/sample-0-origin.html";
        String sampleFile1 = "./samples/sample-1-evil-gemini.html";
        String sampleFile2 = "./samples/sample-2-container-and-clone.html";
        String sampleFile3 = "./samples/sample-3-the-escape.html";
        String sampleFile4 = "./samples/sample-4-the-mash.html";

        main(new String[]{originalFile, sampleFile1, elementId});
        main(new String[]{originalFile, sampleFile2, elementId});
        main(new String[]{originalFile, sampleFile3, elementId});
        main(new String[]{originalFile, sampleFile4, elementId});
    }

    public static void main(String[] args) {

        LOGGER.debug("amount of args: {}", args.length);

        if (args.length < 2) {
            printRunHelpInfo();
            System.exit(0);
        }

        String originalFilePath = args[0];
        String sampleFilePath = args[1];

        String elementId = DEFAULT_ELEMENT_ID;

        if (args.length >= 3) {
            elementId = args[2];
        }

        LOGGER.info("Original file path: {}", originalFilePath);
        LOGGER.info("Sample file path: {}", sampleFilePath);
        LOGGER.info("Element id: {}", elementId);

        XmlAnalyzer xmlAnalyzer = new XmlAnalyzer();
        Optional<ElementMatch> matchingElement = xmlAnalyzer
                .findMatchElement(originalFilePath, sampleFilePath, elementId);

        if (matchingElement.isPresent()) {
            ElementMatch matchElement = matchingElement.get();
            LOGGER.info("Found element, cssSelector: {}", matchElement.getElementHolder().getElement().cssSelector());
            LOGGER.info("Match score: {}%", matchElement.getMatchScore());
            LOGGER.info("Element was selected because of:");
            matchElement.getMatchAnalysis().forEach(LOGGER::info);
        } else {
            LOGGER.error("Element was not found");
        }
    }

    private static void printRunHelpInfo() {
        System.out.println("" +
                "Smart XML analyzer test app.\n" +
                "\n" +
                "Usage: xml-analyzer <input_origin_file_path> <input_other_sample_file_path> [origin-element-id]\n" +
                "\n" +
                "    input_origin_file_path          - full file path to original HTML file\n" +
                "    input_other_sample_file_path    - full file path to sample HTML file\n" +
                "    origin-element-id               - optional target element id, if omitted the default value is: sendMessageButton\n" +
                "\n");
    }

}
